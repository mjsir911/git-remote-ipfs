# `git-remote-ipfs`

This is a simple wrapper for git to pull from ipfs & ipns urls, and push to
ipns urls.


You can access this repository with whatever methods you are currently
accessing it on, but it is also available at
ipfs::/ipns/QmSZVB5buV3s9WFXFqVqEq7cDGtTDZ2kdQNouqRfBNvN5H, accessed through
`git clone ipfs::/ipns/QmSZVB5buV3s9WFXFqVqEq7cDGtTDZ2kdQNouqRfBNvN5H` or using
cloudflare's gateway ` git clone
https://cloudflare-ipfs.com/ipns/QmSZVB5buV3s9WFXFqVqEq7cDGtTDZ2kdQNouqRfBNvN5H`.


I want to promote pinning IPFS repositories


This provides an easier process than https://docs.ipfs.io/how-to/host-git-style-repo/

additional reading: https://github.com/ipfs/in-web-browsers/blob/master/ADDRESSING.md
Prior discussion: https://github.com/ipfs/go-ipfs/issues/1678

also, see https://github.com/ipfs/go-ipfs/issues/3397#issuecomment-284337564
for deduplicating files


see https://rovaughn.github.io/2015-2-9.html for building git remote protocols
